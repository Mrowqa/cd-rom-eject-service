CD-ROM eject service
======================

Simple python script which ejects and closes CD-ROM tray in given timestamp
range when given users are logged in. It could be installed in linux system
as service.

#Features:

* annoys people!

#Configuring

Config files can be found in [config_files/](cd-rom-eject-service/src/master/config_files/)
directory. After service installation these files are moved to /etc/ directory.

#Using

You can run this project in two ways:

1. *Portable mode.* Just run this script:

		service_files/start.sh

2. *Using as service.* Install service:

		sudo ./install.sh
		
	Then you can run service by:
	
		sudo service eject-service start
		
	And make it starting when OS boots:
	
		sudo update-rc.d eject-service defaults

#Updating

If you cloned repository with git, you can download new changes by running

	git pull
	
#Logs

Log file is located in */var/log/eject-service.log*. Log is not rotated or truncated by default,
so it would be good to do some clean up once in a while.

#License

All the code excluding extlib is licenced under [MIT license](cd-rom-eject-service/src/master/LICENCE).






Have fun!

Copyright (C) 2014 Artur "Mrowqa" Jamro, <http://mrowqa.pl>