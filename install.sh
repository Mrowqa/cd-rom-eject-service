#!/bin/bash

INSTALL_PATH=/usr/local/eject-service  # if you change this, change $CHDIR in ./eject-service, too
SERVICE_SCRIPT_DIR=/etc/init.d


if [ $(id -u) -ne 0 ]; then
	echo "Please run as root."
	exit 1
fi


cd $(dirname $0)
mkdir -p $INSTALL_PATH
cp -R ./service_files/* $INSTALL_PATH
cp -R ./config_files/* /etc/
cp ./eject-service $SERVICE_SCRIPT_DIR
chown -R root $SERVICE_SCRIPT_DIR/eject-service /etc/eject-service.* $INSTALL_PATH

echo Installed in: $INSTALL_PATH
echo Please see configuration file in /etc/eject-service.conf.
echo Now you can use \"service eject-service\" command.
