#!/bin/bash


cd $(dirname $0)
if [ "$1" == "-redirected-output" ]; then
	echo "$(date +%d/%b/%Y\ %X): Starting service."
	exec ./eject-service.py
else
	LOGFILE=$1
	if [ "$LOGFILE" != "" ]; then
		exec $0 -redirected-output >> $LOGFILE 2>&1
	else
		exec $0 -redirected-output
	fi
fi
