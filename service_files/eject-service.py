#!/usr/bin/python

import time, re, os.path, random, datetime
from subprocess import *



CONF_FILES_LOCATIONS = [
	"../config_files/",
	"/etc/"
]

CONF_LOCATION = "" # edited by config loading method

USER_FILTER = "" # values: "ALLOW", "DENY" (from user list)
				 # var set by config loader
LIST_OF_CONF_USERS = [] # depends on above option

NEEDED_CONF_VARS = [
	"SLEEP_TIME",
	"EJECT_DURATION"
]

CONF = {
	"SLEEP_TIME": [600.0, 900.0],
	"EJECT_DURATION": [5.0, 5.0]
}


def get_formatted_time():
	return datetime.datetime.now().strftime('%d/%b/%Y %H:%M:%S')

def eject_cd_tray():
	print "%s: ejecting CD-ROM tray" % get_formatted_time()
	return True if call("eject") == 0 else False

	
def close_cd_tray():
	print "%s: closing CD-ROM tray" % get_formatted_time()
	return True if call(["eject", "-t"]) == 0 else False

	
def get_user_list():
	# let the magic begin... 
	return Popen("users", stdout=PIPE).communicate()[0][:-1].split(' ')

	
def is_logged_user_permitted():
	logged_user_list = get_user_list()
	should_be_allowed = USER_FILTER == "ALLOW"
	
	for conf_user in LIST_OF_CONF_USERS:
		for logged_user in logged_user_list:
			if should_be_allowed if re.match("^%s$" % conf_user, logged_user) \
				else not should_be_allowed:
				return True
	return False


# this is so long method... it could be splitted to more methods ;)
def load_conf_files():
	global CONF_LOCATION, USER_FILTER, LIST_OF_CONF_USERS 
	conf_filename = "eject-service.conf"
	for conf_location in CONF_FILES_LOCATIONS:
		conf_path = conf_location + conf_filename
		if os.path.isfile(conf_path):
			with open(conf_path, "r") as conf_file:
			 conf_file_content = conf_file.read()
			 CONF_LOCATION = conf_location
		if CONF_LOCATION != "": break
	
	if CONF_LOCATION == "":
		print "Warning: configuration file not found!"
		return
	
	
	content = conf_file_content.split('\n')
	
	REGEX_TEMPLATE = r"^\s*%s\s*=\s*\"(.*)\""
	REGEX_NUM_OR_RANGE = "^\s*(\d+)\s*(?:\-\s*)?(\d+\s*)?(sec|min|hours)?\s*$"
	time_ratio = {
		"sec": 1,
		"min": 60,
		"hours": 3600,
		None: 60 # default
	}
	
	for line in content:
		for var_name in NEEDED_CONF_VARS:
			match = re.match(REGEX_TEMPLATE % var_name, line)
			if match:
				option_value = match.group(1)
				value_match = re.match(REGEX_NUM_OR_RANGE, option_value)
				if value_match:
					unit_ratio = time_ratio[value_match.group(3)]
					range_down = float(value_match.group(1)) * unit_ratio
					range_up = float(value_match.group(2)) * unit_ratio \
						if value_match.group(2) else range_down
					CONF[var_name] = (range_down, range_up)
					NEEDED_CONF_VARS.remove(var_name)
	
	if NEEDED_CONF_VARS:
		print "Warning: missing options in configuration file: %s" \
			% str(NEEDED_CONF_VARS)
		return

	if os.path.isfile(CONF_LOCATION + "eject-service.allow"):
		USER_FILTER = "ALLOW"
		list_of_users_filename = CONF_LOCATION + "eject-service.allow"
	elif os.path.isfile(CONF_LOCATION + "eject-service.deny"):
		USER_FILTER = "DENY"
		list_of_users_filename = CONF_LOCATION + "eject-service.deny"
	else:
		USER_FILTER = "ALLOW"
		list_of_users_filename = None
		LIST_OF_CONF_USERS = [".*"]
	
	if list_of_users_filename:
		with open(list_of_users_filename, "r") as file:
			LIST_OF_CONF_USERS = file.read().split('\n')

			
def run():
	load_conf_files()
	while True:
		time.sleep(random.uniform(*CONF["SLEEP_TIME"]))
		if is_logged_user_permitted():
			eject_cd_tray()
			time.sleep(random.uniform(*CONF["EJECT_DURATION"]))
			close_cd_tray()
		
		
		
if __name__ == "__main__":
	run()